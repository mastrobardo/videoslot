import Vue from 'vue'
import App from './App.vue'
import api from '@/services/api'
import observer from '@/services/intersectionObserver'
import eventBus from '@/services/eventBus'
import windowResizeMonitor from '@/services/windowResizeMonitor'

Vue.$api = api
Object.defineProperty(Vue.prototype, '$api', {
  get () {
    return api
  }
})

Vue.$observer = observer
Object.defineProperty(Vue.prototype, '$observer', {
  get () {
    return observer
  }
})

Vue.$eventBus = eventBus
Object.defineProperty(Vue.prototype, '$eventBus', {
  get () {
    return eventBus
  }
})

Vue.$windowResizeMonitor = windowResizeMonitor
Object.defineProperty(Vue.prototype, '$windowResizeMonitor', {
  get () {
    return windowResizeMonitor
  }
})

new Vue({
  render: h => h(App),
}).$mount('#app')
