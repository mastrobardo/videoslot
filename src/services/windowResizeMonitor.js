let dimensions = {}
let idx = null;
//TODO: scope this
function setDimensions() {
    dimensions.width = window.screen.availWidth
    dimensions.height = window.screen.availHeight
    window.cancelAnimationFrame(idx)
}
class WindowResizeMonitor {
    constructor () {
    }

    init() {
        window.addEventListener('resize', this.handleResize)
        this.handleResize()
    }

    getDimensions() {
        return dimensions
    }

    handleResize() {
        idx = window.requestAnimationFrame(setDimensions);
    }
}

export default new WindowResizeMonitor()