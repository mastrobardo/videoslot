let observer = null;
const observerOptions = {
    rootMargin: '0px',
    threshold: 1.0,
    root: window.document
}

const callbackMap = new WeakMap()

class Observer {
    constructor () {}

    init() {
        if(!observer) {
            observer = new IntersectionObserver(this.observerCallback, observerOptions)
        }
    }

    add(ele, callback) {
        observer.observe(ele)
        callbackMap.set(ele, callback)
    }

    remove(ele) {
        observer.unobserve(ele)
        callbackMap.delete(ele)
    }

    observerCallback(entries) {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
              const toCall = callbackMap.get(entry.target);
              toCall();
            }
          });
    }
}

export default new Observer()