import axios from 'axios'
import eventBus from './eventBus'

let http = null
let currentResponse = null
let currentPage = null
let itemsPerPage = null
class APIProvider {
  constructor ({ url }) {
    http = axios.create({
      baseURL: url,
      headers: { 'Content-Type': 'application/json' }
    })
  }

  async getList (query = {page: 1, limit: 100}) {
    currentResponse = await http.get('v2/list', {
      params: query
    })
    currentPage = query.page
    itemsPerPage = query.limit
    this.dispatch(currentResponse)
  }

  dispatch(response) {
    const linkHeaders = response.headers.link
    eventBus.$emit('IMAGES_LOADED', response.data, currentPage, itemsPerPage)
    eventBus.$emit('PAGINATION_NEXT', linkHeaders.indexOf('next') > -1)
    eventBus.$emit('PAGINATION_PREV', linkHeaders.indexOf('prev') > -1)
  }
}

export default new APIProvider({
  url: 'https://picsum.photos/'
})