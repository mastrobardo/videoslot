# vs-test

### Some notes about the test     

First of all, im a bit rusty with Vue, so i beg your pardon if im not up to date with file structure/project settings/good practices.   

### Techs    
I sticked with JS and Vue2 ... i realised after that Vue3 has far better options in terms of composition.    
Also, i avoided solutions like Vuetify ( which i used in the past ), as the challenge required. However, im a big fan of material designs. Fot the same reason also, i didnt use VueX, even though, in my opinion, it would have been a good way to manage states and
api responses differences. I sticked to the minimum amount of packages. For lack of time, i couldnt implement tests ( Usually i would have gone directly with TDD as it is easier soemtimes, but it takes more time during development). Please think about this project as a fast POC.    

### Mixins     
You will not find any. I think is an antipattern, and in the past i had some problems with them. WHile plugins might have been a more 'Vue Compliant' solution, i went for services injected on the main Vue instance. As i mentioned above, if i knew Vue3 had refactor completely composition, i'd have use that. I got it a bit too late.   

### Mobile First    
Design is not completely broken on desktop, but some image don't show really well on Desktop. I think mobile first should be a golden rule and the base of all projects. Best view with all new mobile resolution, landscape/portrait.    

### UX    
I'm a crappy designer. While i love good designs ( and to have the right design for the right user, and rules behind), i abandoned UX 20 years ago: i could spend hours on a single  shadow, if left alone. Again, lack of time was a killer.

### IMAGES - API   
I couldn't fins any swagger/documentation for the apis. In particular the first call (list), is loading  FULL SIZE images that are used as  icons... i dont have to explain that this is obviously a bad idea. I implemented a service with an intersection observer for this reason. (threshold is really strict). However, correct size images are requested in the modal, given the available ratio for the screen.  

### DISCLAIMER: 
My main machine is broken, and i did the test on a Windows laptop: i'm really sorry for this. Hence you will find CRLF line ending. Also it seems that, on my machine, eslint + vuture doesnt work really well: i checked files by hand, but i might left some space/tablutaion/alignment behing ( npm run lint is not helping )


